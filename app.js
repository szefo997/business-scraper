const port = (process.env.PORT || 3000);
const log4js = require('log4js');
const log = log4js.getLogger("app");
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const fileUpload = require('express-fileupload');
const index = require('./routes/index');
const RateLimit = require('express-rate-limit');

const app = express();

log4js.configure('./config/log4js.json');

// app.use(logger('dev'));
app.use(log4js.connectLogger(log4js.getLogger("http"), {level: 'auto'}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.enable('trust proxy'); // only if you're behind a reverse proxy (Heroku, Bluemix, AWS if you use an ELB, custom Nginx setup, etc)

//  apply to all requests
app.use(new RateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100, // limit each IP to 100 requests per windowMs
    delayMs: 0 // disable delaying - full speed until the max limit is reached
}));

app.use(bodyParser.json({limit: '1024mb'}));
// to support URL-encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(expressValidator());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload({
    limits: {fileSize: 50 * 1024 * 1024},
    useTempFiles: true,
    tempFileDir: __dirname + '/tmp/',
    safeFileNames: false,
    preserveExtension: false
}));


app.use('/', index);

app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// _clearDb();

app.listen(port, () => {
    // _clearDb();
    log.info(`Started on port ${port}`);
});

module.exports = {app};

