#!/usr/bin/env bash
BUILD_ID=dontKillMe forever -s start --minUptime 1000 --spinSleepTime 1000 --max-old-space-size=4096 app.js