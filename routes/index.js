const {generateXlsx} = require('./save_file');
const express = require('express');
const log = require('log4js').getLogger("index");
const puppeteer = require('puppeteer');
const {createWorker} = require('tesseract.js');

const worker = createWorker({
    logger: (m) => log.info(`tesseract: ${m}`)
});


const URL = 'https://www.baza-firm.com.pl/Ochrona-osób-i-mienia/wielkopolskie/pl/';
// const EMAILS_TO_SEND = ['k.rozycki@onet.pl'];

const router = express.Router();

router.route('/')
    .get(async (req, res) => {
        res.render('index', {
            title: 'Home',
        });
    })
    .post(async (req, res) => {
        const {email} = req.body;
        _scrapeFromBusinessNetReq(email);
        res.redirect('/thank' + (email ? `?email=${email}` : ''));
    });

router.route('/thank')
    .get((req, res) => {
        const email = req.query.email;
        res.render('thank', {
            title: 'Thanks',
            email
        });
    });

async function _getHrefs(page, selector) {
    return await page.$$eval(selector, links => links.map(a => a.href));
}

const _setUserAgent = async (page) => {
    const userAgent = 'Mozilla/5.0 (X11; Linux x86_64)' +
        'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36';
    await page.setUserAgent(userAgent);
}

async function _readEmails(emailElements, page) {
    const emails = [];
    for (const emailElement of emailElements) {
        const imgSrc = await page.evaluate(element => element ? element.src : '', emailElement);
        log.info(`imgSrc: ${imgSrc}`);
        const {data: {text}} = await worker.recognize(imgSrc);
        log.info(`email from image: ${text}`,);
        emails.push(text);
    }
    return emails;
}

async function _readPhones(phonesElements, page) {
    const phones = [];
    for (const phonesElement of phonesElements) {
        phones.push(await page.evaluate(element => element ? element.textContent : '', phonesElement))
    }
    return phones;
}

async function _scrapeFromBusinessNetReq(email) {
    const browser = await _getPuppeteerBrowser();
    const page = await browser.newPage();

    // tesseract for reading email from images
    await worker.load();
    await worker.loadLanguage('eng');
    await worker.initialize('eng');

    await _setUserAgent(page);
    await page.goto(URL, {waitUntil: 'networkidle2'});
    page.on('console', consoleObj => console.log(consoleObj.text()));
    const links = await _getLinksOfAllCompanies(page);
    // const links = ['https://www.baza-firm.com.pl/ochrona-osób-i-mienia/śrem/agencja-ochrony-osób-i-mienia-lion/pl/174703.html'];
    // const links = ['https://www.baza-firm.com.pl/ochrona-osób-i-mienia/kaźmierz/biuro-ochrony-magielski-patrol-piotr-magielski/pl/147618.html'];

    const scrapedComapnies = [];


    for (const link of links) {
        await _wait(Math.random() * 1000);
        await page.goto(link, {waitUntil: 'networkidle2'});
        const nameElement = await page.$('[itemprop="name"]');
        const streetElement = await page.$('[itemprop="streetAddress"]');
        const zipCodeElement = await page.$('[itemprop="postalCode"]');
        const cityElement = await page.$('[itemprop="addressLocality"]');
        const voivodeshipElement = await page.$('[itemprop="addressRegion"]');
        const phonesElements = await page.$$('[itemprop="telephone"]');
        const wwwELement = await page.$('[itemprop="url"]');
        const emailElements = await page.$$('#emlAddrBox img');

        const phones = await _readPhones(phonesElements, page);
        const emails = await _readEmails(emailElements, page);
        const street = await page.evaluate(element => element ? element.textContent : '', streetElement);
        const zipCode = await page.evaluate(element => element ? element.textContent : '', zipCodeElement);
        const city = await page.evaluate(element => element ? element.textContent : '', cityElement);
        const company = new CompanyBuilder()
            .setName(await page.evaluate(element => element ? element.textContent : '', nameElement))
            .setAddress(`${street}, ${zipCode}, ${city}`)
            .setVoivodeship(await page.evaluate(element => element ? element.textContent : '', voivodeshipElement))
            .setPhones(phones.join(', '))
            .setWww(await page.evaluate(element => element ? element.textContent : '', wwwELement))
            .setPageFromScraped(link)
            .setEmails(emails.join(', '))
            .build();
        log.info('company', company);
        scrapedComapnies.push(company);
    }
    await generateXlsx(scrapedComapnies);
}

async function _getLinksOfAllCompanies(page) {
    const hrefs = [];
    const paginationHrefs = await _getHrefs(page, '#resBtNav .pagination a');
    paginationHrefs.pop();
    log.info('paginationHrefs', paginationHrefs);
    hrefs.push(...await _getHrefs(page, '#leftColumn .pikto_txt.cursorPointer'));
    for (const href of paginationHrefs) {
        await _wait(Math.random() * 1000);
        await page.goto(href, {waitUntil: 'networkidle2'});
        const nextHrefs = await _getHrefs(page, '#leftColumn .pikto_txt.cursorPointer');
        hrefs.push(...nextHrefs);
    }
    log.info('hrefs', hrefs);
    log.info('hrefs.length', hrefs.length);
    return hrefs;
}

async function _getPuppeteerBrowser() {
    return await puppeteer.launch({
        args: ['--netifs-to-ignore=INTERFACE_TO_IGNORE'],
        headless: true,
        slowMo: 250,
        // timeout: 60000,
        defaultViewport: {
            width: 1920,
            height: 1080
        }
    });
}

function _sendEmail(to, shopName, file) {
}

async function _wait(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms);
    });
}

class Company {
    constructor(
        builder
    ) {
        this.name = builder.name;
        this.address = builder.address;
        this.voivodeship = builder.voivodeship;
        this.county = builder.county;
        this.tinNumber = builder.tinNumber;
        this.yearOfFoundation = builder.yearOfFoundation;
        this.phones = builder.phones;
        this.industries = builder.industries;
        this.emails = builder.emails;
        this.wwww = builder.wwww;
        this.pageFromScraped = builder.pageFromScraped;
    }
}

class CompanyBuilder {
    constructor() {
    }

    setName(name) {
        this.name = name;
        return this;
    }

    setAddress(address) {
        this.address = address;
        return this;
    }

    setVoivodeship(voivodeship) {
        this.voivodeship = voivodeship;
        return this;
    }

    setCounty(county) {
        this.county = county;
        return this;
    }

    setTinNumber(tinNumber) {
        this.tinNumber = tinNumber;
        return this;
    }

    setYearOfFoundation(yearOfFoundation) {
        this.yearOfFoundation = yearOfFoundation;
        return this;
    }

    setPhones(phones) {
        this.phones = phones;
        return this;
    }

    setIndustries(industries) {
        this.industries = industries;
        return this;
    }

    setEmails(emails) {
        this.emails = emails;
        return this;
    }


    setWww(www) {
        this.wwww = www;
        return this;
    }

    setPageFromScraped(pageFromScraped) {
        this.pageFromScraped = pageFromScraped;
        return this;
    }

    build() {
        return new Company(this);
    }
}


module.exports = router;
