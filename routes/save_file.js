const log = require('log4js').getLogger("save_file");
const XLSX = require('xlsx');
const fs = require('fs');

function _buildExcelData(companiesFromWww) {
    const companiesData = [
        ['Nazwa firmy', 'Adres', 'Województwo', 'Numery telefonów', 'Adresy e-mail', 'Strona www firmy', 'Strona z której pobrano dane']
    ];
    companiesFromWww.forEach((comapny) => {
        companiesData.push([
            comapny.name,
            comapny.address,
            comapny.voivodeship,
            comapny.phones,
            comapny.email,
            comapny.wwww,
            comapny.pageFromScraped,
        ]);
    });
    return companiesData;
}

async function generateXlsx(companiesFromWww) {
    const companiesData = _buildExcelData(companiesFromWww);
    const wb = XLSX.utils.book_new();
    const wsCompanies = XLSX.utils.aoa_to_sheet(companiesData);
    const currentDate = new Date().toISOString().slice(0, 10);
    XLSX.utils.book_append_sheet(wb, wsCompanies, `Dane ${currentDate}`);
    const filename = `dane_firm_${currentDate}.xlsx`;
    const file = XLSX.write(wb, {type: 'buffer', bookType: 'xlsx', bookSST: false});
    _saveXlsx(filename, file);
}

function _saveXlsx(filename, file) {
    log.info(`saveXlsx: ${filename}`)
    fs.writeFile(filename, file, (err) => {
        if (err) {
            return log.error(`saveXlsx error: ${err}`);
        }
        log.info("The file was saved!");
    });
}

module.exports = {generateXlsx};
